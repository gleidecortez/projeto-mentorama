# projeto-mentorama

import com.sun.source.tree.WhileLoopTree;

import java.util.Scanner;

public class Menu1 {

    public static void main(String[] args) {

        while( true ) {

            Scanner menu = new Scanner(System.in);

            System.out.println("!       MENU       !");
            System.out.println("!   OPÇÕES         !");
            System.out.println("!   1. OPÇÃO 1     !");
            System.out.println("!   2. OPÇÃO 2     !");
            System.out.println("!   3. SAIR        !");

            System.out.println("Selecione uma opção:");
            int opcao = menu.nextInt();

            if (opcao == 3) {
                System.out.println("O programa foi encerrado");
            }
            switch (opcao) {
                case 1:
                    System.out.println("Você escolheu a primeira opção");
                    break;
                case 2:
                    System.out.println("Você escolheu a segunda opção");
                    break;
            }
        }
    }
}